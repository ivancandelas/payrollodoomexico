
from odoo import _, api, fields, models
from odoo.exceptions import ValidationError


class HrSalaryRuleCategory(models.Model):
    _name = "hr.salary.rule.category"
    _description = "Salary Rule Category"

    name = fields.Char(required=True, translate=True)
    code = fields.Char()
    salary_rules_ids = fields.One2many(
        "hr.salary.rule", "category_id", string="Salary Rule Categories"
    )
    note = fields.Text(string="Description")
    company_id = fields.Many2one(
        "res.company",
        string="Company",
        default=lambda self: self.env.company,
    )