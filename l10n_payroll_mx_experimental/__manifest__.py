{
    "name": "Payroll Mexico Experimental",
    "version": "15.0.1.0.0",
    "summary": "Manage employee paystubs",
    'description': """
Payroll Management Mexico
=========================
This module allos to manage the payroll for employees in compliance with mexican laws.

    """,
    "category": "Human Resources",
    "website": "",
    "author": "Ivan Candelas",
    "license": "AGPL-3",
    "installable": True,
    "application": False,
    "depends": [
        "hr_contract",
        "mail"
    ],
    "data": [
    ],
}
