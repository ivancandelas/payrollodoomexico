
from odoo import models, fields, api, _
from odoo.exceptions import UserError


class Loan(models.Model):
    _name = 'loan'
    _description = 'Loans'

    name = fields.Char(required=True)
    reference = fields.Char()
    date = fields.Date(required=True, default=fields.Date.context_today)
    amount = fields.Float(digits="Loan", required=True)
    installment_qty = fields.Integer(string='Installment Quantity', required=True)
    installment_amount = fields.Float(digits="Loan", compute="_compute_installment_amount")
    paid_amount = fields.Float(digits="Loan", readonly=True, compute="_compute_paid_amount")
    pending_amount = fields.Float(digits="Loan", readonly=True)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('active', 'Active'),
        ('paid', 'Paid'),
        ('cancel', 'Cancelled'),
    ], string="Status", required=True, default='draft', copy=False)
    cancel_reason = fields.Char()
    payment_ids = fields.One2many('loan.payment', 'loan_id', string='Payments')

    def action_confirm(self):
        print("Confirm")

    @api.depends('amount', 'installment_qty')
    def _compute_installment_amount(self):
        for loan in self:
            loan.installment_amount = (loan.amount / loan.installment_qty) if loan.installment_qty else 0.0

    @api.depends('payment_ids')
    def _compute_paid_amount(self):
        total_paid = 0.0
        for loan in self:
            for line in loan.payment_ids:
                if line.state == "applied":
                    total_paid += line.amount
            balance_amount = loan.amount - total_paid
            loan.pending_amount = balance_amount
            loan.paid_amount = total_paid


class LoanPayment(models.Model):
    _name = 'loan.payment'
    _description = 'Loan Payments'

    date = fields.Date()
    amount = fields.Float(digits="Loan")
    state = fields.Selection([
        ('draft', 'Draft'),
        ('applied', 'Applied'),
        ('cancel', 'Cancelled'),
    ], default='draft', required=True)
    loan_id = fields.Many2one('loan', string='Loan', ondelete='cascade', index=True)
    reference = fields.Char()
    origin = fields.Char()

    sequence_number = fields.Integer(string='#', compute='_compute_sequence_number', help='Line Numbers')

    @api.depends('loan_id')
    def _compute_sequence_number(self):
        """Function to compute line numbers"""
        for loan in self.mapped('loan_id'):
            sequence_number = 1
            for lines in loan.payment_ids:
                lines.sequence_number = sequence_number
                sequence_number += 1

    def unlink(self):
        for line in self:
            if line.state != "draft":
                raise UserError(
                    _(
                        "You are not allowed to delete a credit control "
                        "line that is not in draft state."
                    )
                )
        return super().unlink()
