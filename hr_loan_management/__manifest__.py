{
    'name': 'HR Loan Management',
    'version': '15.0.0.1.0',
    'description': 'Loan Management',
    'summary': '',
    'author': 'Ivan Candelas',
    'website': '',
    'license': 'LGPL-3',
    'category': '',
    'depends': [
        'base'
    ],
    'data': [
        'data/hr_loan_data.xml',
        'views/loan_views.xml',
        'security/loan_security.xml',
        'security/ir.model.access.csv',
    ],
    'auto_install': False,
    'application': False,
}
