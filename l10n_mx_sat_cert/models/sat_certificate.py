
import base64
import tempfile
from datetime import datetime

from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.x509.oid import ExtensionOID
from cryptography.x509.oid import NameOID

from odoo import models, fields, api, tools
from odoo.exceptions import UserError

CERT_TYPE = [
    ('csd', 'CSD'),
    ('fiel', 'FIEL')
]


class SatCertificate(models.Model):
    _name = 'sat.certificate'
    _description = 'SAT Certificates'

    name = fields.Char(string="Certificate name", required=True)
    display_name = fields.Char(compute='_compute_display_name')
    cert_type = fields.Selection(CERT_TYPE, string='Certificate type', readonly=True)

    subject = fields.Char(string='Subject', readonly=True)
    rfc = fields.Char(string='RFC', readonly=True)
    serial_number = fields.Char(string="Serial number", readonly=True)
    validity_not_before = fields.Datetime(string="Validity not before", readonly=True)
    validity_not_after = fields.Datetime(string="Validity not after", readonly=True)

    cert_content = fields.Binary(string="Certificate file")
    cert_content_name = fields.Char('Certificate file name')

    key_content = fields.Binary(string="Certificate key")
    key_content_name = fields.Char('Certificate key name')

    key_password = fields.Char(string="Certificate key password")

    # company_id = fields.Many2one('res.company', string='Company', required=True, readonly=True,
    #                             default=lambda self: self.env.company)

    @api.depends('name', 'cert_type', 'serial_number')
    def _compute_display_name(self):
        for cert in self:
            cert.display_name = '%s (%s: %s)' % (cert.name,
                                                 dict(cert._fields['cert_type'].selection).get(cert.cert_type),
                                                 cert.serial_number)

    @api.constrains('cert_content', 'key_content', 'key_password', 'name')
    def _validate_certificate(self):

        for cert in self:

            cer = base64.decodebytes(self.cert_content)
            obj = x509.load_der_x509_certificate(cer, default_backend())

            _not_before = obj.not_valid_before
            _not_after = obj.not_valid_after

            cert.validity_not_after = _not_after
            cert.validity_not_before = _not_before

            cert.serial_number = f'{obj.serial_number:x}'[1::2]

            _subject = obj.subject.get_attributes_for_oid(NameOID.ORGANIZATION_NAME)[0].value

            cert.subject = _subject
            cert.rfc = obj.subject.get_attributes_for_oid(NameOID.X500_UNIQUE_IDENTIFIER)[0].value.split('/')[0].strip()

            _is_fiel = obj.extensions.get_extension_for_oid(ExtensionOID.KEY_USAGE).value.key_agreement

            if _is_fiel:
                cert.cert_type = 'fiel'
            else:
                cert.cert_type = 'csd'

            cb64 = base64.b64encode(self.cert_content).decode()
            print(cb64)

            # COMMAND = f'openssl pkcs8 -inform DER -in {prv} -passin pass:12345678a -out nombreLlave.pem'

            # with open('a', 'r') as a:
            #    a.close()

            print("@@@@@")
            print(obj.subject.get_attributes_for_oid(NameOID.SERIAL_NUMBER)[0].value.split('/')[1].strip())
            print("@@@@@")

            now = datetime.utcnow()
            _is_valid_time = (now > _not_before) and (now < _not_after)
            msg = ''
            if not _is_valid_time:
                msg = 'El certificado no es vigente'
                raise UserError(msg)

            _cer_modulus = obj.public_key().public_numbers().n

            print(_not_before)
            print(_not_after)
            print(_is_fiel)
            print(_cer_modulus)

            print(obj)
            # print(_serial_number)
            print(self.key_password.encode())
            try:
                kobj = serialization.load_der_private_key(base64.decodebytes(self.key_content), self.key_password.encode(), default_backend())
            except ValueError:
                msg = 'La contraseña es incorrecta'
                raise UserError(msg)

            _key_modulus = kobj.public_key().public_numbers().n
            _are_couple = _cer_modulus == _key_modulus
            if not _are_couple:
                msg = 'El CER y el KEY no son par'
                raise UserError(msg)

            with tempfile.NamedTemporaryFile(prefix='pk_', suffix='.key') as der_file:
                der_file.write(b'This is a named temporary file.')
                print(der_file.name)

            # pub = certificate.get_pubkey()

    def sign(self, data, password=''):
        private_key = self._get_key(password)
        firma = private_key.sign(data, padding.PKCS1v15(), hashes.SHA256())
        return base64.b64encode(firma).decode()

    @property
    @tools.ormcache()
    def cert_as_pem(self):

        self.ensure_one()

        cer = base64.decodebytes(self.cert_content)
        obj = x509.load_der_x509_certificate(cer, default_backend())
        _cer_pem = obj.public_bytes(serialization.Encoding.PEM).decode()
        _cer_txt = ''.join(_cer_pem.split('\n')[1:-2])

        return _cer_txt
