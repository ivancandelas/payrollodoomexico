{
    'name': 'Mexico Localization SAT Certificate management',
    'version': '15.0.0.1.0',
    'description': 'SAT Certificates management',
    'summary': '',
    'author': 'Ivan Candelas',
    'website': '',
    'license': 'LGPL-3',
    'category': '',
    'depends': [
        'base'
    ],
    'data': [
        'views/menus.xml',
        'views/sat_certificate_views.xml',
        'views/company_views.xml',
        'security/ir.model.access.csv'
    ],
    'auto_install': False,
    'application': False,
}
