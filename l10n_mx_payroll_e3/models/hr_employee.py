
import datetime
import re


from odoo import fields, models, api, _
from odoo.tools.misc import ustr
from odoo.exceptions import ValidationError


class HrEmployeeBase(models.AbstractModel):
    _inherit = "hr.employee.base"

    rfc = fields.Char("RFC")
    curp = fields.Char("CURP")


class HrEmployee(models.Model):
    _inherit = "hr.employee"

    __check_rfc_mx_re = re.compile(br"(?P<initials>[A-Za-z\xd1\xf1&]{4})"
                                   br"[ \-_]?"
                                   br"(?P<year>[0-9]{2})(?P<month>[01][0-9])(?P<day>[0-3][0-9])"
                                   br"[ \-_]?"
                                   br"(?P<unique>[A-Za-z0-9&\xd1\xf1]{3})$")

    @api.constrains('rfc')
    def check_rfc(self):
        for employee in self:
            if employee.rfc and self._validate_rfc(employee.rfc) is False:
                employee_label = _("Employee [%s]", employee.name)
                msg = employee._build_rfc_error_message(employee.rfc, employee_label)
                raise ValidationError(msg)

    def _validate_rfc(self, rfc):
        rfc = ustr(rfc).encode('iso8859-1')
        m = self.__check_rfc_mx_re.match(rfc)
        if not m:
            return False
        try:
            year = int(m.group('year'))
            if year > 30:
                year = 1900 + year
            else:
                year = 2000 + year
            datetime.date(year, int(m.group('month')), int(m.group('day')))
        except ValueError:
            return False

        return True

    @api.model
    def _build_rfc_error_message(self, wrong_rfc, record_label):

        rfc_label = _("RFC")

        expected_format = "'AAAA######CCC' (AAAA=Initials, ######=Birth Date, CCC=Unique Code)"

        return '\n' + _(
            'The %(rfc_label)s number [%(wrong_rfc)s] for %(record_label)s does not seem to be valid. \nNote: the expected format is %(expected_format)s',
            rfc_label=rfc_label,
            wrong_rfc=wrong_rfc,
            record_label=record_label,
            expected_format=expected_format,
        )
