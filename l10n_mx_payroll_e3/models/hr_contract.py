
import logging
import math

from odoo import fields, models, api, _
from odoo.exceptions import UserError, ValidationError

_logger = logging.getLogger(__name__)

IDSE_SALARY_TYPE = [
    ('0', 'Fijo'),
    ('1', 'Variable'),
    ('2', 'Mixto'),
]

IDSE_EMPLOYEE_TYPE = [
    ('1', 'Permanente'),
    ('2', 'Eventual'),
    ('3', 'De la contrucción'),
    ('4', 'Del campo'),
]

IDSE_PERIOD_WORK_TYPE = [
    ('0', 'Jornada Normal'),
    ('1', 'Un día'),
    ('2', 'Dos días'),
    ('3', 'Tres días'),
    ('4', 'Cuatro días'),
    ('5', 'Cinco días'),
    ('6', 'Jornada reducida'),
]


class HrMxContractType(models.Model):
    _inherit = "hr.contract.type"

    l10n_mx_sat_contract_type = fields.Many2one('sat.nomina.c_tipo_contrato',
                                                string='SAT Contract Type')


class HrMxContract(models.Model):

    _inherit = "hr.contract"
    _description = 'HR Contract'

    salary_type = fields.Selection(IDSE_SALARY_TYPE, string='Salary Type',
                                   required=True, default='2')
    employee_type = fields.Selection(IDSE_EMPLOYEE_TYPE, string='Employee type',
                                     required=True, default='1')
    work_period_type = fields.Selection(IDSE_PERIOD_WORK_TYPE, string='Period work type',
                                        required=True, default='0')

    wage_type = fields.Selection(selection=[('day', 'Daily'), ('month', 'Monthly')],
                                 string='Salary Type', required=True, default='month')

    custom_wage = fields.Monetary('Custom Wage', tracking=1,
                                  help="Employee's custom gross wage.")

    daily_wage = fields.Monetary('Daily Wage', compute='_compute_daily_wage')

    hourly_wage = fields.Monetary('Hourly Wage', compute='_compute_hourly_wage')

    compound_wage = fields.Monetary('Daily Compound Wage', readonly=True,
                                    tracking=2, compute="_get_compound_wage")

    fixed_compound_wage = fields.Monetary('Daily Compound Wage Fixed Portion', readonly=True,
                                          tracking=3, compute="_get_compound_wage")

    variable_compound_wage = fields.Monetary('Daily Compound Wage Variable Portion', readonly=True,
                                             tracking=4, compute="_get_compound_wage")

    employeer_register_id = fields.Many2one('payroll.employeer.register',
                                            string='Employeer Register', required=True)

    payroll_group_id = fields.Many2one('payroll.group',
                                       string='Payroll Group', required=True)

    salary_history_ids = fields.One2many('payroll.work.history', 'contract_id', string='Salary History')

    l10n_mx_tipo_regimen_id = fields.Many2one('sat.nomina.c_tipo_regimen',
                                              string='Regimen Type', required=True)

    l10n_mx_tipo_jornada_id = fields.Many2one('sat.nomina.c_tipo_jornada',
                                              string='Shift Type', required=True)

    union_state = fields.Selection([('unionized', 'Unionized'),
                                    ('non_unionized', 'Non Unionized'),],
                                   string="Union State", default='non_unionized',
                                   required=True)

    @api.depends('payroll_group_id', 'daily_wage')
    def _get_compound_wage(self):
        for rec in self:
            days = (fields.Date.context_today(self) - rec.date_start).days + 1
            ages = days / 365.0
            ages = math.ceil(ages)
            line = rec.payroll_group_id.line_ids.filtered(lambda w: w.age == ages)
            factor = 1.0
            if line:
                factor = line[0].compound_factor
            rec.fixed_compound_wage = rec.daily_wage * factor
            rec.variable_compound_wage = 120.50
            rec.compound_wage = rec.fixed_compound_wage + rec.variable_compound_wage

    @api.onchange('custom_wage','wage_type')
    def _onchange_custom_wage(self):
        _logger.info("CHANGING SALARY")
        for record in self:
            if record.wage_type == 'day':
                record.wage = record.custom_wage * record.payroll_group_id.days_month_factor
            if record.wage_type == 'month':
                record.wage = record.custom_wage

    @api.depends('wage')
    def _compute_daily_wage(self):
        _logger.info("COMPUTING DAILY")
        for record in self:
            record.daily_wage = (record.wage / record.payroll_group_id.days_month_factor) if record.payroll_group_id else 0.0

    @api.depends('wage')
    def _compute_hourly_wage(self):
        _logger.info("COMPUTING HOURLY")
        for record in self:
            record.hourly_wage = ((record.wage / record.payroll_group_id.days_month_factor) / 8) if record.payroll_group_id else 0.0

    def _update_wage(self, values):
        if 'custom_wage' in values:
            wage_type = self.wage_type

            if 'wage_type' in values:
                wage_type = values['wage_type']

            if wage_type == 'day':
                values['wage'] = values['custom_wage'] * self.payroll_group_id.days_month_factor
            if wage_type == 'month':
                values['wage'] = values['custom_wage']
        return values

    def _create_work_history(self, vals):
        if vals.get('state') == 'open':
            self.env['payroll.work.history'].sudo().create({
                'contract_id': self.id,
                'type': "admission",
                'daily_wage': self.daily_wage,
                'compound_wage': self.compound_wage,
                'date': self.date_start
            })
        if vals.get('state') == 'close':
            self.env['payroll.work.history'].sudo().create({
                'contract_id': self.id,
                'type': "termination",
                'daily_wage': self.daily_wage,
                'compound_wage': self.compound_wage,
                'date': self.date_end
            })

    def write(self, vals):
        vals = self._update_wage(vals)
        res = super(HrMxContract, self).write(vals)
        self._create_work_history(vals)
        return res

    @api.model
    def create(self, values):
        if 'state' in values and values['state'] != 'draft':
            raise UserError(_('New contracts always should be draft'))
        values = self._update_wage(values)
        res = super(HrMxContract, self).create(values)
        return res

    @api.constrains('company_id','employeer_register_id')
    def check_valid_reg_code(self):
        for record in self:
            if record.company_id != record.employeer_register_id.company_id:
                raise ValidationError('Employeer register must be from the same company')
