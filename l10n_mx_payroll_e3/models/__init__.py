from . import (employer_register,
               payroll_base,
               hr_contract,
               payroll_group,
               work_history,
               hr_leave_type,
               hr_employee,
               hr_payslip,
               hr_payslip_line,
               hr_salary_rule,
               hr_job,
               res_company)
