
from odoo import models, fields


class SATListTipoConcepto(models.Model):
    _name = "sat.list.tipo.concepto"

    name = fields.Char('Description')
    code = fields.Char('Code')
    type = fields.Selection([
        ('percepcion', 'Percepcion'),
        ('deduccion', 'Deduccion'),
        ('otro_pago', 'Otro Pago')
    ])
    active = fields.Boolean()

    _sql_constraints = [
        ('type_code_unique',
         'unique(type, code)',
         'The code type combination should be unique')
    ]
