import re

from odoo import fields, models, _
from odoo import api
from odoo.tools.misc import ustr
from odoo.exceptions import ValidationError

EMPLOYEER_REGISTER_CLASS = [
    ('I','I - Ordinario'),
    ('II','II - Bajo'),
    ('III','III - Medio'),
    ('IV','IV - Alto'),
    ('V','V - Máximo'),
]


class EmployeerRegister(models.Model):
    _name = 'payroll.employeer.register'
    _description = "Employeer Register"
    _rec_name = 'code'

    name = fields.Char('Name', required=True)

    code = fields.Char('Register Code', required=True, size=11)
    display_name = fields.Char(compute='_compute_display_name')
    activity = fields.Char('Activity', size=64)
    classification = fields.Selection(EMPLOYEER_REGISTER_CLASS, string='Class',
                                      required=True)
    fraction = fields.Char('Fraction', size=3)
    company_id = fields.Many2one('res.company', string='Company', required=True,
                                 default=lambda self: self.env.company)

    risk_grades_ids = fields.One2many(
        string="Risk Grades",
        comodel_name="payroll.employeer.risk.grade",
        inverse_name="employeer_register_id")

    _sql_constraints = [
        ('code_uniq',
         'unique(code)',
         'An employeer register could be defined only one time.')
    ]

    __check_code_re = re.compile(br"(?P<municipio>[A-Za-z]{1})" \
                                br"(?P<cmun>[0-9]{2})(?P<clave>[0-9]{5})" \
                                br"(?P<modalidad>[0-9]{2})(?P<verifica>[0-9]{1})$")

    @api.depends('name', 'code')
    def _compute_display_name(self):
        for register in self:
            register.display_name = _("%s (%s)", register.name, register.code)

    @api.constrains('code')
    def check_register_code(self):
        for register in self:
            code = ustr(register.code).encode('iso8859-1')
            m = self.__check_code_re.match(code)
            if not m:
                raise ValidationError('Invalid format for register code')


class RiskGrade(models.Model):
    _name = 'payroll.employeer.risk.grade'
    _description = 'Employeer risk grade'
    _order = 'name desc'

    name = fields.Char('Year', required=True)
    date = date = fields.Date(string='Date', required=True, index=True,
                              default=fields.Date.context_today)
    grade = fields.Float('Risk Grade', digits=(7, 5), required=True)

    employeer_register_id = fields.Many2one(
        'payroll.employeer.register', string='Employeer Register', index=True,
        ondelete='cascade', required=True)

    _sql_constraints = [
        ('risk_grade_uniq',
         'unique (employeer_register_id,name)',
         'Duplicate years in grade risks not allowed !')
    ]
