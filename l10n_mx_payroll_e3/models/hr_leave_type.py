
from odoo import models, fields


class HolidaysType(models.Model):
    _inherit = "hr.leave.type"

    ss_leave_type = fields.Selection([
        ('sick', 'Sick Leave'),
        ('absence', 'Absence'),
        ('other', 'Other')
    ], string='Social security leave type'
    )

    payment_type = fields.Selection([
        ('paid', 'Paid'),
        ('unpaid', 'Unpaid'),
        ('allocation', 'Allocation')
    ], string='Tipo de pago'
    )