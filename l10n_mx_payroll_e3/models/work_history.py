
from odoo import fields, models

# En esta tabla se debe registrar:
# Movimiento tipo 01/08 cuando se crea el contrato
# Movimiento tipo 07 cuando se modifica el salario
# Movimiento tipo 02 cuando se termina el contrato

HISTORY_TYPE = [
    ('admission', 'Admission'),          # 01 Ingreso
    ('termination', 'Termination'),      # 02 Baja
    ('salary_change', 'Salary change'),  # 07 Modificación salarial
    ('readmission', 'Readmission')       # 08 Reingreso
]


class PayrollWorkHistory(models.Model):
    _name = 'payroll.work.history'
    _description = 'Work history'

    _order = "date"

    # el numero de seguro social debe ir asociado al empleado
    contract_id = fields.Many2one('hr.contract', required=True)
    date = fields.Date()
    type = fields.Selection(HISTORY_TYPE, string='Type', required=True)
    daily_wage = fields.Monetary(string='Daily Wage', required=True)
    compound_wage = fields.Monetary(string='Daily Compound Wage', required=True)
    currency_id = fields.Many2one(string="Currency", related='contract_id.currency_id', readonly=True)

    _sql_constraints = [
        (
            'unique_contract_date',
            'UNIQUE(contract_id, date)',
            'Cannot duplicate date'
        ),
    ]
