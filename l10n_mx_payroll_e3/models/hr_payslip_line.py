
from odoo import models


class HrPayslipLine(models.Model):
    _inherit = "hr.payslip.line"

    def _get_sat_code(self):
        self.ensure_one()
        if self.salary_rule_id.type == 'allowance':
            sat_code = self.salary_rule_id.sat_allowance_type_id.code
        elif self.salary_rule_id.type == 'deduction':
            sat_code = self.salary_rule_id.sat_deduction_type_id.code
        elif self.salary_rule_id.type == 'other_payment':
            sat_code = self.salary_rule_id.sat_other_payment_type_id.code
        else:
            sat_code = 'code not set'

        return sat_code
