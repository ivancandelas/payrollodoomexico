
from odoo import fields, models


class HrSalaryRule(models.Model):
    _inherit = "hr.salary.rule"

    type = fields.Selection([
        ('allowance', 'Allowance'),
        ('deduction', 'Deduction'),
        ('other_payment', 'Other Payment'),
        ('auxiliar', 'Auxiliar')
    ], required=True, default='auxiliar')

    sat_allowance_type_id = fields.Many2one("sat.nomina.c_tipo_percepcion",
                                            string="SAT Allowance Type")

    sat_deduction_type_id = fields.Many2one("sat.nomina.c_tipo_deduccion",
                                            string="SAT Deduction Type")

    sat_other_payment_type_id = fields.Many2one("sat.nomina.c_tipo_otro_pago",
                                                string="SAT Other Payment Type")
