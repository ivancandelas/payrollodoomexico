
from odoo import models, fields


class HRJob(models.Model):
    _inherit = "hr.job"

    l10n_mx_sat_position_risk = fields.Many2one('sat.nomina.c_riesgo_puesto',
                                                string='SAT Position Risk')
