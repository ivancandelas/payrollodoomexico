
# from datetime import datetime
# import pytz
from dateutil import relativedelta

from odoo import models, fields, api


class HrPayslip(models.Model):
    _inherit = "hr.payslip"

    issue_date = fields.Datetime(string='Issue Date', copy=False, readonly=True)

    seniority = fields.Char("Seniority", compute="_compute_seniority", store=True)

    currency_id = fields.Many2one(string="Currency", related='contract_id.currency_id', readonly=True)

    daily_wage = fields.Monetary('Daily Wage')

    compound_wage = fields.Monetary('Daily Compound Wage')

    total_allowances = fields.Monetary('Total Allowances')
    total_other_payments = fields.Monetary('Total Other Payments')
    total_perceptions = fields.Monetary('Total Perceptions')

    total_deductions = fields.Monetary('Total Deductions')

    total_net = fields.Monetary('Total Net')

    @api.depends('date_payment', 'contract_id.date_start')
    def _compute_seniority(self):
        for rec in self:

            date_diff = relativedelta.relativedelta(rec.date_payment, rec.contract_id.date_start)
            # Construir la cadena de texto en formato "PnYnMnD"
            seniority = 'P'
            if date_diff.years != 0:
                seniority += f'{date_diff.years}Y'
            if date_diff.months != 0:
                seniority += f'{date_diff.months}M'
            if date_diff.days != 0:
                seniority += f'{date_diff.days}D'

            # Si todas las unidades son cero, se asume 0 días de antigüedad
            if seniority == 'P':
                seniority += '0D'

            rec.seniority = seniority

    def _compute_worked_days(self, contract, day_from, day_to):

        result = super(HrPayslip, self)._compute_worked_days(contract, day_from, day_to)
        print(result)
        result['name'] = "Dias Trabajados"
        result['code'] = "P100"
        return result

    def create_cfdi(self):
        self.ensure_one()
        self.recompute()

        self.issue_date = fields.Date.context_today(self)
        self._compute_seniority()

        qweb = self.env['ir.qweb']

        allowances = self.line_ids.search([('slip_id', '=', self.id), ('salary_rule_id.type', '=', 'allowance')])
        deductions = self.line_ids.search([('slip_id', '=', self.id), ('salary_rule_id.type', '=', 'deduction')])

        values = {
            'o': self,
            'allowances': allowances,
            'deductions': deductions
        }

        cfdi = qweb._render('l10n_mx_payroll_e3.cfdi40_nomina12', values=values)

        print("######################### START CREATING CFDI")
        print(cfdi)
        with open('cfdi_nominas.xml', 'w') as f:
            f.write(cfdi)
        print("######################### END CREATING CFDI")

    def issue_date_tz_iso(self):
        return fields.Datetime.context_timestamp(self, self.issue_date).replace(tzinfo=None).isoformat()

    def compute_sheet(self):
        print("EEEEEPALEEE")
        for payslip in self:
            payslip.daily_wage = payslip.contract_id.daily_wage
            payslip.compound_wage = payslip.contract_id.compound_wage
            payslip._compute_seniority()

            lines = payslip.line_ids.filtered(lambda x: x.salary_rule_id.type == 'allowance')
            payslip.total_allowances = sum(lines.mapped('total'))

            lines = payslip.line_ids.filtered(lambda x: x.salary_rule_id.type == 'deduction')
            payslip.total_deductions = sum(lines.mapped('total'))

            lines = payslip.line_ids.filtered(lambda x: x.salary_rule_id.type == 'other_payment')
            payslip.total_other_payments = sum(lines.mapped('total'))

            payslip.total_perceptions = (payslip.total_allowances + payslip.total_other_payments)
            payslip.total_net = (payslip.total_allowances + payslip.total_other_payments - payslip.total_deductions)

        return super(HrPayslip, self).compute_sheet()
