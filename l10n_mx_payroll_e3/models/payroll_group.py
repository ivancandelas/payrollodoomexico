
from odoo import fields, models, api, _


class PayrollGroup(models.Model):
    _name = 'payroll.group'
    _description = "Payroll Group"
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char('Group Name', required=True)
    description = fields.Text('Description')
    active = fields.Boolean(string="Activo", default=True)
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.company)
    line_ids = fields.One2many('payroll.group.line', 'group_id', string="Lines")

    days_month_factor = fields.Float('Days per Month Factor', digits=(4,2), default=30.4, required=True, tracking=True)

    _sql_constraints = [
        ('company_name_unique',
         'unique(company_id, name)',
         'Choose another name - it has to be unique per company.'),
        ('check_days_per_month',
         'CHECK(days_month_factor > 0)',
         'Days per month cannot be 0')
    ]


class PayrollGroupLine(models.Model):
    _name = 'payroll.group.line'
    _description = 'Payroll Group Line'
    _order = "group_id, age"

    @api.depends('age')
    def _compute_name(self):
        for rec in self:
            rec.name = _('Group: ') + (rec.group_id.name or '')  + _(' - Age:') + str(rec.age)

    @api.depends('vacation_days', 'vacation_bonus', 'yearly_bonus_days')
    def _get_compound_factor(self):
        for rec in self:
            year_days = 365.0
            vacation_bonus_days = rec.vacation_days * (rec.vacation_bonus / 100.0)
            total_days = year_days + vacation_bonus_days + rec.yearly_bonus_days
            rec.compound_factor = total_days / year_days

    name = fields.Char('Description', store=True, compute='_compute_name')
    group_id = fields.Many2one('payroll.group', string="Payroll Group", required=True, ondelete="cascade")

    age = fields.Integer('Age', required=True, index=True)

    vacation_days = fields.Integer('Vacation days', required=True, default=0)
    vacation_bonus = fields.Float(string="% Vacation bonus", default=25, digits=(8,2))

    yearly_bonus_days = fields.Integer('Yearly bonus days', required=True, default=15)

    compound_factor = fields.Float(string="Compound factor", compute="_get_compound_factor", digits=(12,8))
    company_id = fields.Many2one(related="group_id.company_id", store=True)

    _sql_constraints = [
        ('company_group_age_uniq', 'unique(company_id, group_id, age)', 'Group + Age must be unique'),
        ('check_yearly_bonus_days', 'CHECK(yearly_bonus_days >=15)', 'Yearly bonus days cannot be less than 15'),
        ('check_vacation_bonus', 'CHECK(vacation_bonus >=25)', 'Vacation bonus percent cannot be less than 25%'),
    ]
