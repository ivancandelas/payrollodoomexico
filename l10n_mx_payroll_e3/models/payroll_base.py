from odoo import fields, models


class PayrollMinimumWage(models.Model):
    _name = 'payroll.minimum.wage'
    _description = 'Payroll Minimum Wage'
    _order = 'effective_date desc'

    name = fields.Char(string='Year', required=True, default='/')
    effective_date = fields.Date(string='Effective date', required=True, index=True,
                                 default=fields.Date.context_today)
    amount = fields.Float('Minimum Wage Amount', digits=(6, 2), required=True)
    north_border_amount = fields.Float('Minimum Wage North Border Amount', digits=(6, 2), required=True)

    _sql_constraints = [
        ('name_unique',
         'unique(name)',
         'Choose another value - it has to be unique!')
    ]


class PayrollUMA(models.Model):
    _name = 'payroll.uma'
    _description = 'UMA'
    _order = 'effective_date desc'

    name = fields.Char(string='Year', required=True, default='/')
    effective_date = fields.Date(string='Effective date', required=True, index=True,
                                 default=fields.Date.context_today)
    amount = fields.Float('Amount', digits=(6, 2), required=True)

    _sql_constraints = [
        ('name_unique',
         'unique(name)',
         'Choose another value - it has to be unique!')
    ]


class PayrollUMI(models.Model):
    _name = 'payroll.umi'
    _description = 'UMI'
    _order = 'effective_date desc'

    name = fields.Char(string='Year', required=True, default='/')
    effective_date = fields.Date(string='Effective date', required=True, index=True,
                                 default=fields.Date.context_today)
    amount = fields.Float('Amount', digits=(6, 2), required=True)

    _sql_constraints = [
        ('name_unique',
         'unique(name)',
         'Choose another value - it has to be unique!')
    ]
