
from odoo import models, fields


class ResCompany(models.Model):
    _inherit = 'res.company'

    l10n_mx_fiscal_regime_id = fields.Many2one('sat.cfdi.c_regimen_fiscal',
                                               string='Fiscal Regime')

    l10n_mx_payroll_cert_id = fields.Many2one('sat.certificate', string='Payroll Certificate')
