{
    "name": "Rules for payroll mexican localization OCA",
    "version": "15.0.1.0.0",
    "summary": "Create basic rules for mexican payroll lozalization",
    "description": """
Payroll Salary Rules Mexico
===========================
Load basic salary rules for payroll mexican localization.

    """,
    "category": "Human Resources",
    "website": "",
    "author": "Ivan Candelas",
    "license": "AGPL-3",
    "installable": True,
    "application": False,
    "depends": [
        "payroll"
    ],
    "data": [
        'data/hr_payroll_data.xml'
    ],
}
